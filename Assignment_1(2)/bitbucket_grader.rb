require 'fileutils'
require 'yaml'

repo_folder = './repo'
temp_folder = './temp'

def clone_app(url, repo_folder)
  repo_cleaned = clean_repo_folder(repo_folder)
  system("git clone '#{url}' #{repo_folder}") && repo_cleaned
end

def clean_repo_folder(repo_folder)
  begin
    FileUtils.rm_rf(repo_folder)
    FileUtils.mkdir(repo_folder)
    FileUtils.chmod(0777, repo_folder)
  rescue
    false
  end
  true
end

def move_important_contents(repo_folder, temp_folder, items)
  items.each do |item|
    FileUtils.rm_rf(File.join(temp_folder, item))
    FileUtils.cp_r(File.join(repo_folder, item), File.join(temp_folder, item))
  end
end

def run_rspec_tests(temp_folder)
  FileUtils.rm_rf(File.join(temp_folder, 'results.txt'))
  `cd #{temp_folder};
   rvm use 2.2.3;
   rake db:drop RAILS_ENV=test;
   rake db:migrate RAILS_ENV=test;
   rspec --format progress --out results.txt;
   rspec --format documentation --out results_doc.txt`
  begin
    first_line = File.open(File.join(temp_folder, 'results.txt'), &:gets)
    passes     = first_line.count('.')
    fails      = first_line.count('F')
    total      = passes + fails
    puts       'Total number of rspec tests: ' + total.to_s
    grade      = 100.0 * passes / total
  rescue
  end
  grade ||= 'error'
end

# Main routine
feedback = ''
score    = 0
begin
  if clone_app(ARGV[0], repo_folder)
    score += 20
    if ['app', 'db', ['config', 'routes.rb']].all? { |file| File.exists?(File.join(repo_folder, *file)) }
      move_important_contents(repo_folder, temp_folder, ['app', 'db', File.join('config', 'routes.rb')])
      score += 20
      rspec_grade = run_rspec_tests(temp_folder)
      if rspec_grade.to_s != 'error'
        puts 'Rspec Grade: ' + rspec_grade.to_s
        score += (0.6 * rspec_grade)
        if score == 100
          grade_stat = 'G'
        else
          grade_stat = '1'
          feedback << File.read(File.join(temp_folder, "results_doc.txt"))
        end
      else
        grade_stat = '2'
        puts 'Error running RSPEC tests!'
        FileUtils.rm_rf(File.join(temp_folder, 'rspec_error_out.txt'))
        `cd #{temp_folder};
               rvm use 2.2.3;
               rspec 2>>rspec_error_out.txt`
        errorline = File.open(File.join(temp_folder, 'rspec_error_out.txt'), &:gets) rescue ''
        feedback << " The following error, returned by Rails, may help you in debugging your submission.\nERROR: " + errorline unless errorline.empty?
      end
    else
      grade_stat = '3'
      puts 'Error when moving items.'
    end
  else
    grade_stat = '4'
    puts 'Error when cloning the repository.'
  end
rescue
  grade_stat = '6'
  puts 'Other Error'
end
feedback << YAML.load(File.read("feedback.yml"))[grade_stat]
File.open("feedback.txt", "w") { |file| file.write(feedback) }
puts feedback
