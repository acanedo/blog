require 'rails_helper'

describe "comments/index" do
  before(:each) do
    assign(:comments, [
      stub_model(Comment, post_id: 1, body: "MyText"),
      stub_model(Comment, post_id: 1, body: "MyText")
    ])
  end

  it "renders a list of comments" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", text: '', count: 2
    assert_select "tr>td", text: "MyText", count: 2
  end
end
